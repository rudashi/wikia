<?php

namespace Rudashi\Wikia\Request;

use Illuminate\Foundation\Http\FormRequest;

class WikiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'title' => 'required|unique:wiki,title,' . $this->id,
            'menu_title' => 'required|unique:wiki,menu_title,' . $this->id,
            'content' => 'required',
        ];
    }
}
