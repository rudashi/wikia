<?php

namespace Rudashi\Wikia\Model;

use App\User;
use App\Model\Model;
use App\Model\Contracts\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Panoscape\History\HasHistories;

class Wikia extends Model
{
    use SoftDeletes,
        ModelTrait,
        HasHistories;

    protected $table = 'wiki';

    protected $fillable = [ 'active' ];

    public function user() : \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function children() : \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id') ;
    }

    public  function getModelLabel() : string
    {
        return $this->title;
    }
}
