<?php

namespace Rudashi\Wikia;

use Illuminate\Support\ServiceProvider;
use Rudashi\Wikia\Repositories\WikiaRepository;

class WikiaServiceProvider extends ServiceProvider
{

    const NAMESPACE = 'wikia';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configure();
        $this->loadViewsFrom(__DIR__ . '/resources/views', self::NAMESPACE);
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path(self::NAMESPACE.'.php'),
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/'.self::NAMESPACE),
            __DIR__ . '/database/migrations' => database_path('migrations'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/'.self::NAMESPACE),
        ], self::NAMESPACE.'-views');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/config.php', self::NAMESPACE);
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->app->make(WikiaController::class);
    }

    private function configure()
    {
        view()->composer('layout.home', function (\Illuminate\View\View $view) {
            $this->extendMenu($this->makeMenu($view), $view);
        });
    }

    private function makeMenu(\Illuminate\View\View $view) : array
    {
        try {
            $configMenu = config(self::NAMESPACE.'.menu');
            $posts = (new WikiaRepository())->getPublishedPostsForMenu();
            if ($posts->isNotEmpty()) {
                $configMenu[0]['children'] = $posts->map(function($item){
                    return [
                        'name' => $item->menu_title ?? 'undefined',
                        'route' => route('wiki.show', $item->id),
                        'route-prefix' => 'wiki',
                        'icon-class' => $item->icon ?? 'fa-circle-o',
                        'children' => [],
                    ];
                })->toArray();
            }
            if (auth()->user() !== null && auth()->user()->can('wiki-add')) {
                array_unshift($configMenu[0]['children'], [
                    'name' => 'Posts List',
                    'route' => route('wiki.index'),
                    'prefix' => 'wiki',
                    'icon-class' => 'fa-wikipedia-w',
                    'children' => [],
                ] );
            }
            return make_menu($configMenu, $view->offsetGet('currentUser'));
        } catch (\Error $e) {
            return [];
        }
    }

    private function extendMenu(array $data, \Illuminate\View\View $view)
    {
        if (!empty($data)) {
            if ($view->offsetGet('menu')->contains('route', self::NAMESPACE)) {
                $view->offsetGet('menu')->map(function ($item) use ($data) {
                    if ($item->route === self::NAMESPACE) {
                        $item = $data;
                    }
                    return $item;
                });
            } else {
                $view->offsetSet('menu', $view->offsetGet('menu')->merge(collect($data)));
            }
        }
    }
}
