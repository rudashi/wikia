<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web', 'auth']], function() {

    Route::group(['prefix' => 'wiki'], function() {

        Route::get('/', function() {
            return view('wikia::index');
        })->name('wiki.index');
        Route::get('data', 'Rudashi\Wikia\WikiaController@data')->name('wiki.data');
        Route::post('reorder', 'Rudashi\Wikia\WikiaController@reorder')->name('wiki.reorder');

        Route::get('create', 'Rudashi\Wikia\WikiaController@create')->middleware('permission:wiki-add')->name('wiki.create');
        Route::post('create', 'Rudashi\Wikia\WikiaController@store')->middleware('permission:wiki-add')->name('wiki.store');
        Route::get('{id}', 'Rudashi\Wikia\WikiaController@show')->name('wiki.show')->where('id', '[0-9]+');
        Route::get('{id}/edit', 'Rudashi\Wikia\WikiaController@edit')->middleware('permission:wiki-modify')->name('wiki.edit')->where('id', '[0-9]+');
        Route::patch('{id}/edit', 'Rudashi\Wikia\WikiaController@update')->middleware('permission:wiki-modify')->name('wiki.update')->where('id', '[0-9]+');
        Route::delete('{id}', 'Rudashi\Wikia\WikiaController@destroy')->middleware('permission:wiki-delete')->name('wiki.destroy')->where('id', '[0-9]+');

        Route::group(['middleware' => 'permission:wiki-active'], function() {
            Route::get('{id}/active', 'Rudashi\Wikia\WikiaController@active')->name('wiki.active')->where('id', '[0-9]+');
            Route::get('{id}/inactive', 'Rudashi\Wikia\WikiaController@inactive')->name('wiki.inactive')->where('id', '[0-9]+');
        });
        Route::post('image-upload', 'Rudashi\Wikia\WikiaController@uploadImage')->name('wiki.image.upload');
    });

});
