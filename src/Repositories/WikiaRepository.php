<?php

namespace Rudashi\Wikia\Repositories;

use Illuminate\Support\Facades\DB;
use Rudashi\Wikia\Model\Wikia;
use App\Repositories\Contracts\Repository;
use Illuminate\Http\Request;

/**
 * Repozytorium wikipedii.
 * @property    \Rudashi\Wikia\Model\Wikia|\Illuminate\Database\Eloquent\Builder $model
 */
class WikiaRepository extends Repository
{

    public function model() : string
    {
        return Wikia::class;
    }

    /**
     * @param   int $id
     * @param   array $columns
     * @return  \Rudashi\Wikia\Model\Wikia|\Illuminate\Support\MessageBag
     */
    public function findId(int $id = 0, array $columns = ['*'])
    {
        $data = $this->model->withTrashed()->find($id, $columns);

        if ($id === 0) {
            return $this->error(400, __('No post id have been given.'));
        }

        if ($data === null) {
            return $this->error(400, __('Given id :code is invalid or post not exist.', ['code' => $id]));
        }

        if ($data->deleted_at !== null) {
            return $this->error(400, __('Post with given id :code is deleted.', ['code' => $id]));
        }

        return $data;
    }

    public function getPublished() : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->where('active', 1)->orderBy('order')->get();
    }

    public function getPublishedPostsForMenu()
    {
        return $this->model->where('active', 1)->exclude(['content'])->orderBy('order')->get();
    }

    public function store(Request $request, int $id = 0) : Wikia
    {
        $post = ($id === 0) ? $this->model : $this->find($id);

        $post->user_id      = auth()->id();
        $post->order        = $this->getNextOrderNumber();
        $post->title        = $request->input('title');
        $post->menu_title   = $request->input('menu_title');
        $post->content      = clean($request->input('content'));

        $post->save();

        return $post;
    }

    public function getNextOrderNumber() : int
    {
        return (int) DB::select('SHOW TABLE STATUS LIKE "wiki"')[0]->Auto_increment;
    }
}