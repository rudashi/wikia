@permission('wiki-modify')
<a class="btn btn-xs btn-warning" href="{{ route('wiki.edit', $id) }}" title="{{__('Edit')}}"><i class="fa fa-pencil"></i></a>
@endpermission
<a class="btn btn-xs btn-info" href="{{ route('wiki.show', $id) }}" title="{{__('Show')}}"><i class="fa fa-eye"></i></a>
@permission('wiki-active')
@if ($active === 0)
    <a class="btn btn-xs btn-default" href="{{ route('wiki.active', $id) }}" title="{{__('Publish')}}"><i class="fa fa-unlock"></i></a>
@else
    <a class="btn btn-xs btn-default" href="{{ route('wiki.inactive', $id) }}" title="{{__('Pending')}}"><i class="fa fa-lock"></i></a>
@endif
@endpermission
@permission('wiki-delete')
<form method="post" action="{{ route('wiki.destroy', $id) }}" style="display: inline;">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="button" class="btn btn-xs btn-danger"  title="{{__('Delete')}}"><i class="fa fa-times-circle"></i></button>
</form>
@endpermission