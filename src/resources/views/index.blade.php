@extends('wikia::panel')

@section('title', __('Totem Wikia'))

@section('content')
    <div class="row">
        <div id="DTAA" class="col-lg-9 col-md-8 col-sm-7 col-xs-7">
            @permission('wiki-add')
            <a class="btn btn-sm btn-success" href="{{ route('wiki.create') }}" title="{{__('Create New')}}"><i class="fa fa-plus-circle"></i> <span class="hidden-sm">{{ __('Create New') }}</span></a>
            @endpermission
            @permission('wiki-modify')
            <a class="btn btn-sm btn-warning btn-off disabled" href="{{ route('wiki.edit', 0) }}" title="{{__('Edit')}}" data-type="modify"><i class="fa fa-pencil"></i> <span class="hidden-sm">{{ __('Edit') }}</span></a>
            @endpermission
            @permission('wiki-delete')
            <form method="post" action="{{ route('wiki.destroy', 0) }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="button" class="btn btn-sm btn-danger btn-off disabled"  title="{{__('Delete')}}"><i class="fa fa-times-circle"></i> <span class="hidden-sm">{{ __('Delete') }}</span></button>
            </form>
            @endpermission
            <a class="btn btn-sm btn-info btn-off disabled" href="{{ route('wiki.show', 0) }}" title="{{__('Show')}}" data-type="preview"><i class="fa fa-eye"></i> <span class="hidden-sm">{{ __('Show') }}</span></a>
            @permission('wiki-active')
            <a class="btn btn-sm btn-default btn-off disabled" id="activeButton" href="{{ route('wiki.active', 0) }}" title="{{__('Publish')}}"><i class="fa fa-unlock"></i> <span class="hidden-sm">{{ __('Publish') }}</span></a>
            <a class="btn btn-sm btn-default btn-off disabled" id="inactiveButton" href="{{ route('wiki.inactive', 0) }}" title="{{__('Pending')}}"><i class="fa fa-lock"></i> <span class="hidden-sm">{{ __('Pending') }}</span></a>
            @endpermission
        </div>
        @include('layout.dataTable.search')
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table id="wiki-table" class="panel panel-primary table table-striped table-bordered dataTable" width="100%">
                <thead class="panel-heading">
                    <tr>
                        <th data-toggle-cols></th>
                        <th>{{ __('Title') }}</th>
                        <th>{{ __('Title of Menu') }}</th>
                        <th>{{ __('Author') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th data-toggle-cols class="col-xs-1">{{ __('Actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('layout.dialog-delete')

@component('layout.dataTable.config', [ 'rowReorder' => true ])
    <script>
        $(function(){

            /**
             * DataTable
             */
            let table = $.admin.dataTable('#wiki-table', {
                ajax: '{{ route('wiki.data') }}',
                columns: [
                    { data: 'sequence', orderable: false, width: '1%' },
                    { data: 'title', orderable: false },
                    { data: 'menu_title', orderable: false },
                    { data: 'user', orderable: false, name: 'user.firstname' },
                    { data: 'status', orderable: false, name: 'active' },
                    { data: 'action', orderable: false, searchable: false}
                ],
                rowReorder: {
                    update: false,
                    dataSrc: 'order'
                }
            });

            /**
             * Zmiana kolejności wierszy.
             */
            table.init.on( 'row-reorder', function ( event, diff ) {
                let data = {};
                for (let i=0; i < diff.length; i++) {
                    data[i] = {
                        id: table.init.row(diff[i].node).data().id,
                        order: diff[i].newData
                    };
                }

                $.admin.ajax.post({
                    url     : '{{ route('wiki.reorder') }}',
                    _token  : $('meta[name="csrf-token"]').attr('content'),
                    data    : { data: data }
                }, '{{ auth()->user()->getApiToken() }}');
            });
        });
    </script>
@endcomponent