@extends('wikia::panel')

@section('title', __('Totem Wikia'))

@section('content')
    <div class="row">
        <section class="content-header clearfix">
            <h1>{{ __('Create New Post') }}</h1>
            @component('layout.breadcrumb', [ 'data' => [ 'Wikia' => 'wiki.index', 'Creating' => '' ] ])@endcomponent
        </section>
        @component('layout.alert')
            @include('layout.errors')
        @endcomponent
        <form class="form-horizontal" method="POST" action="{{ route('wiki.store') }}">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="box box-default">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-3 control-label">{{__('Title')}}</label>
                                    <div class="col-sm-7">
                                        <input class="form-control required" id="title" name="title" placeholder="{{__('Title')}}" value="{{ old('title') }}" maxlength="50" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="menu_title" class="col-sm-3 control-label">{{__('Title of Menu')}}</label>
                                    <div class="col-sm-7">
                                        <input class="form-control required" id="menu_title" name="menu_title" placeholder="{{__('Title of Menu')}}" value="{{ old('menu_title') }}" maxlength="50" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <textarea class="form-control no-resize-y" id="content" name="content" rows="50" title="" required>{{ old('content') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="form-group">
                            <b class="col-xs-5 control-label">{{ __('Status') }}:</b>
                            <div class="col-xs-5">
                                <p class="form-control-static">{{ 'Pending' }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-danger" href="{{ route('wiki.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                        <button class="btn btn-primary pull-right"><i class="fa fa-check"></i> {{ __('Confirm') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts-inline')
    <script>
        $(function(){

            $('#content').trumbowyg({
                lang: 'pl',
                btnsDef: {
                    image: {
                        dropdown: ['insertImage', 'upload'],
                        ico: 'insertImage'
                    }
                },
                btns: [
                    ['formatting'],
                    ['strong', 'em', 'del', 'foreColor'],
                    ['superscript', 'subscript'],
                    ['link', 'image'],
                    ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                    ['orderedList','unorderedList'],
                    ['horizontalRule'],
                    ['removeformat'],
                    ['viewHTML'],
                    ['fullscreen']
                ],
                plugins: {
                    upload: {
                        serverPath : '{{ route('wiki.image.upload') }}',
                        fileFieldName: 'file_data',
                        headers: {
                            'X-CSRF-TOKEN': $('[name="csrf-token"]').attr('content')
                        },
                        urlPropertyName: 'data.url'
                    }
                }
            });

        });
    </script>
@endpush