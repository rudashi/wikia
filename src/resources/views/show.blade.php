@extends('wikia::panel')

@section('title', __('Totem Wikia').' '.$data->title)

@section('content')
    <div class="row">
        <section class="content-header clearfix">
            <h1>{{ $data->title }}</h1>
            @component('layout.breadcrumb', [ 'data' => [ 'Wikia' => 'wiki.index', 'Show' => '' ] ])@endcomponent
        </section>
        <div class="col-xs-12">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="box box-default">
                    <div class="box-body">
                        {!! $data->content !!}
                    </div>
                    <div class="box-footer">
                        @permission('wiki-modify')
                        <a class="btn btn-warning pull-right" href="{{ route('wiki.edit',$data->id) }}"><i class="fa fa-pencil"></i> {{ __('Edit') }}</a>
                        @endpermission
                        <a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection