<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Wiki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        try{
            Schema::create('wiki', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->integer('user_id')->unsigned();
                $table->integer('parent_id')->unsigned()->nullable();
                $table->integer('order')->nullable();
                $table->string('title');
                $table->string('menu_title');
                $table->text('content')->nullable();
                $table->tinyInteger('active')->default(0);
                $table->softDeletes();

                $table->foreign('parent_id')->references('id')->on('wiki')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }


        Artisan::call('db:seed', [
            '--class' => \Rudashi\Wikia\Database\Seeds\WikiaPermissionSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = new \Rudashi\Wikia\Database\Seeds\WikiaPermissionSeeder();
        $permissions->down();

        Schema::dropIfExists('wiki');
    }
}
