<?php

namespace Rudashi\Wikia\Database\Seeds;

use \Illuminate\Database\PermissionsContractSeeder;

class WikiaPermissionSeeder extends PermissionsContractSeeder
{
    public function permissions()
    {
        return [
            [
                'name' => 'wiki',
                'description' => 'Display Wiki Posts Listing',
            ],
            [
                'name' => 'wiki-add',
                'description' => 'Create New Wiki Post',
            ],
            [
                'name' => 'wiki-modify',
                'description' => 'Edit Wiki Post',
            ],
            [
                'name' => 'wiki-delete',
                'description' => 'Delete Wiki Post',
            ],
            [
                'name' => 'wiki-active',
                'description' => 'Activate Wiki Post',
            ],
        ];
    }
}
