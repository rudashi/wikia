<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu item
    |--------------------------------------------------------------------------
    |
    | This is the array of menu item in sidebar of system.
    |
    */
    'menu' => [
        [
            'key' => 'wiki',
            'name' => 'Wikia',
            'route' => 'wiki',
            'route-prefix' => 'wiki',
            'icon-class' => 'fa-wikipedia-w',
            'children' => [

            ],
        ],
    ],

];