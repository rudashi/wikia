<?php

namespace Rudashi\Wikia;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageUploadRequest;
use App\Repositories\FileRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\MessageBag;
use Rudashi\Wikia\Model\Wikia;
use Rudashi\Wikia\Repositories\WikiaRepository;
use Rudashi\Wikia\Request\WikiaRequest;
use Yajra\DataTables\DataTables;

class WikiaController extends Controller
{

    public function __construct(WikiaRepository $repository, Redirector $redirect)
    {
        $this->model = $repository;
        $this->redirect = $redirect;
    }

    public function data(DataTables $dataTables) : \Illuminate\Http\JsonResponse
    {
        $data = $this->model->with('user')->exclude(['content'])->orderBy('order');

        return $dataTables->eloquent($data)
            ->setRowClass(function (Wikia $wiki) {
                return $wiki->active === 0 ? 'inactive' : '';
            })
            ->editColumn('user', function(Wikia $wiki) {
                return $wiki->user->fullname;
            })
            ->addColumn('status', function(Wikia $wiki) {
                return __($wiki->active === 0 ? 'Pending' : 'Published');
            })
            ->addColumn('sequence', '<i class="fa fa-arrows move-able"></i>')
            ->addRowAttr('data-id', '{{$id}}')
            ->addColumn('action', 'wikia::data-action')
            ->rawColumns(['action', 'sequence'])
            ->make();
    }

    public function reorder(Request $request) : \Illuminate\Http\JsonResponse
    {
        $sequence = collect($request->input('data'));

        if ($sequence->isNotEmpty()) {
            $this->model->updateValues('order', $sequence);
        }
        return response()->json( $sequence );
    }

    public function uploadImage(ImageUploadRequest $request) : string
    {
        try {
            $data = (new FileRepository)->setPath('wiki')->store($request);

            $response = [
                'url' => asset('storage/' .$data->name),
                'success' => true
            ];

        } catch (\Exception $exception) {

            $response = [
                'error' => $exception->getMessage(),
                'success' => false
            ];

        } catch (\Throwable $exception) {
            $response = [
                'error' => $exception->getMessage(),
                'success' => false
            ];
        }

        return json_encode($response);
    }

    public function create() : \Illuminate\View\View
    {
        return view('wikia::create');
    }

    public function edit(int $id = 0)
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->errorResponse($data->first('message'), 'wiki.index');
        }

        return view('wikia::edit')->with([
            'data' => $data,
        ]);
    }

    public function show(int $id = 0)
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->errorResponse($data->first('message'));
        }

        return view('wikia::show')->with([
            'data' => $data,
        ]);
    }

    public function store(WikiaRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request);

        return $this->successResponse(__('Wiki Post :name created successfully.', ['name' => $data->title]), 'wiki.index' );
    }

    public function update(WikiaRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request, $id);

        return $this->successResponse(__('Wiki Post :name updated successfully.', ['name' => $data->title]), 'wiki.index' );
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->errorResponse($data->first('message'), 'wiki.index');
        }

        $this->model->delete($id);

        return $this->successResponse(__('Wiki Post :name deleted successfully.', ['name' => $data->title]), 'wiki.index' );
    }

    public function active(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->errorResponse($data->first('message'), 'wiki.index');
        }

        $data->activate();

        return $this->successResponse(__('Post :name published successfully', ['name' => $data->title]) );
    }

    public function inactive(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->errorResponse($data->first('message'), 'wiki.index');
        }

        $data->deactivate();

        return $this->successResponse(__('Post :name is now pending for a review.', ['name' => $data->title]) );
    }

}
