# Simple Wiki for SAM in Laravel 5

This package is created to handle posts a'la wikipedia. 

## General System Requirements
- [PHP >7.0.0](http://php.net/)
- [Laravel 5.5](https://github.com/laravel/framework)
- SAM >2.1.6


## Quick Installation
```bash
$ composer require rudashi/wikia:"~1.0"
```

#### Service Provider (Optional on Laravel 5.5)
Register provider on your `config/app.php` file.
```php
'providers' => [
    ...,
    Rudashi\Wikia\WikiaServiceProvider::class,
]
```

#### Configuration
```bash
$ php artisan vendor:publish --provider=Rudashi\Wikia\WikiaServiceProvider
```

##### Warning !
After update package don't forget to update **Views** by publish or manually make changes.
```bash
$ php artisan vendor:publish --tag=wikia-views
```
And that's it!

## Authors

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)